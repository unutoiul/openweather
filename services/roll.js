import random from '../utils/random';

const roll = (diceCount, faceCount, modifier = 0) => {
	if(typeof diceCount  === 'string'){
		let split = diceCount.split('d');
		let operator = (diceCount.indexOf('-') === -1)?'+':'-';
		
		diceCount = Number(split[0]);
		faceCount = Number(split[1].split(operator)[0]);
		modifier = (operator==='-') ? 0-Number(split[1].split(operator)[1]) : Number(split[1].split(operator)[1]);
	}

	let max = diceCount * faceCount + modifier;
	let min = (modifier === 0)? diceCount : diceCount + modifier;
	
 	let randomNumber = random(min, max); 
	
	return randomNumber;
}

export default roll;