import random from '../utils/random';

var diceCount = 1;
var faceCount = 6;
var modifier = 0;

var min = 1;
var max = 1;

var generateNumber = () => {
	min = (modifier === 0) ? 1 : diceCount + modifier;
	max = diceCount * faceCount + modifier;

	return random(min, max);
}

var createRoller = (arg_diceCount, arg_faceCount, arg_modifier) => {
	if(!arg_diceCount){
		return random(min, max);
	}

	diceCount = arg_diceCount;
	faceCount = arg_faceCount;
	modifier = Number(arg_modifier);
	
	return generateNumber;
}

generateNumber.toDiceNotation = () => {
	modifier = (modifier>0) ? '+' + modifier : modifier;
	return `${diceCount}d${faceCount}${modifier}`; 
}

export default createRoller;