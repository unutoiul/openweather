const Route = {
  	method: 'GET',
  	path: '/',
  	handler: (request, reply) => {
  		return reply.view('index');
  	}
};

export default Route;