var babel = require('babel/register');
var server = require('./server.js');

server.start((err) => {
	console.log('Server is running on: ', server.info.uri);
});