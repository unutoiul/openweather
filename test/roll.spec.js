import roll from './../services/roll';
import { expect } from 'chai';

const diceCount = 3;
const faceCount = 8;
const modifier = -2;

const diceNotation = "3d8-2";
const diceNotationPositive = "3d8+2";

describe('/services/roll', () => {
	it('returns a random number between 3-24 - test with 2 parameters (diceCount, faceCount)', (done) => {
		let result = roll(diceCount, faceCount);

		expect(result).to.be.a('number');
		expect(result).to.within(diceCount, faceCount * diceCount);
		done();
	});

	
	it('returns a random number between 1-22 - test with 3 parameters (diceCount, faceCount, modifier)', (done) => {
		let result = roll(diceCount, faceCount, modifier);

		expect(result).to.be.a('number');
		expect(result).to.within(diceCount+modifier, faceCount * diceCount + modifier);
		done();
	});


	it('returns a random number between 1-22 - test with 1 parameter (string) dice notation format(negative modifier)', (done) => {
		let result = roll(diceNotation);

		expect(result).to.be.a('number');
		expect(result).to.within(diceCount + modifier, faceCount * diceCount + modifier);
		done();
	});	

	it('returns a random number between 5-26 - test with 1 parameter (string) dice notation format(positive modifier)', (done) => {
		let result = roll(diceNotationPositive);

		expect(result).to.be.a('number');
		expect(result).to.within(diceCount - modifier, faceCount * diceCount - modifier);
		done();
	});

});