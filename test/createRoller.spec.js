import createRoller from './../services/createRoller';
import { expect } from 'chai';

const diceCount = 3;
const faceCount = 8;
const modifier = -2;

const diceNotation = "3d8-2";
const diceNotationPositive = "3d8+2";

describe('/services/createRoller', () => {

	it('check if returns ok', (done) => {
		let result = new createRoller(diceCount, faceCount, modifier);

		expect(result).to.be.a('function');
		expect(result()).to.be.a('number');
		expect(result.toDiceNotation()).to.be.a('string');

		done();
	});


	it('returns a whole number between 1 and 22', (done) => {
		let result = new createRoller(diceCount, faceCount, modifier);

		expect(result()).to.within(diceCount+modifier, faceCount * diceCount + modifier);
		
		done();
	});	

	it('returns 3d8-2 (negative modifier)', (done) => {
		let result = new createRoller(diceCount, faceCount, modifier);

		expect(result.toDiceNotation()).to.equal(diceNotation);

		done();
	});	

	it('returns 3d8+2 (positive modifier)', (done) => {
		let result = new createRoller(diceCount, faceCount, -modifier);

		expect(result.toDiceNotation()).to.equal(diceNotationPositive);

		done();
	});
});


