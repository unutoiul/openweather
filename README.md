# Dice Notation

The project is using Hapi.js to route and handlebars to render the HTML. 

Is using semantic-ui for styling.

## Requirements

This project requires node v7.  This is required to support es6 babel-core.

## Setup
`npm install`


This starts the node process with `nodemon` to restart the server on any changes.

## Running Locally

`npm start`

## Improvements
- Write Unit Tests. (need more time)
- Write Integration tests. (need more time)
- Code coverage
- Format data better
- Create UI using React or Angular 2.0