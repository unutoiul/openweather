import Hapi from 'hapi';

import fs from 'fs';
import path from 'path';
import util from 'util';
import handlebars from 'handlebars';
import vision from 'vision';
import inert from 'inert';

let server = new Hapi.Server();

server.connection({
  	port: '8000'
});

//Register Plugins
server.register([vision, inert], (err) => {
    if(err){
        throw err;
    }
});

server.views({
    engines: {
        html: handlebars
    },
 	relativeTo: __dirname,
    path: 'views',
    partialsPath: 'views/partials'
});

// Load routes directory recursively
let requireDir = (dir) => {
	for (let p of fs.readdirSync(dir)) {
		let fpath = path.resolve(dir, p);
		let stat = fs.lstatSync(fpath);
		if (stat.isDirectory()) {
			requireDir(fpath);
		} else if (stat.isFile()) {
			let route = require(fpath);
			server.route(route);
		}
	}
};

requireDir(path.join(__dirname, '/routes'));

export default server;